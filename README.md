# Geos 64 Programming

### Overview

This respository contains a number of C64 Geos programs that were created with the cc65 compiler/assembler package on Ubuntu Linux to demonstrate how Geos (released in 1987) on the C64 can be programmed today (in 2018) on Linux (and probably other operating systems). The programs in this repository show, how many of the Geos C API functions included in the cc65 package can be used beyond what is demonstrated in the cc65 Geos example programs.

The directories in this repository contain the following:

**_010-geos-owl-demo-1_**: A first program that demonstrates the use of the following APIs:

 * Menus and Menu handlers
 * Dialog Box call
 * Sprite and sprite movement
 * Output text
 * Draw lines and rectangles
 * Uses GEOS process function to periodically call a function that, e.g. moves the sprite to a new position on the screen
 * React to keyboard input in a handler function
 * Use of some functions of the C stdlib, e.g. to convert an int to a string.

**_020-geos-owl-demo-2_**: Extension of Owl Demo One with the use of the following APIs:

 * Detection of mouse button click/release events (otherPressVec)
 * Get mouse X and Y position, check if button was pressed/released, act only on press

**_More things to come soon..._**
