/**
 * A demo program to show how the cc65 C API functions for GEOS 64
 * can be used.
 *
 * This program uses, among others, the following GEOS API functions 
 * to demonstrate their use:
 *
 *  - Menus and Menu handlers
 *  - Dialog Box call
 *  - Sprite and sprite movement
 *  - Output text
 *  - Draw lines and rectangles
 *  - Uses GEOS process function to periodically call a function
 *    that, e.g. moves the sprite to a new position on the screen
 *  - React to keyboard input
 *  - Use of some functions of the C stdlib, e.g. to convert
 *    and int to a string.
 *
 * @version    1.0 2018-08-15
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
               martin.sauter@wirelessmoves.com
 * @license    GNU General Public License
 * @since      Since Release 1.0
 *
 */


#include <geos.h>
#include <stdlib.h>

// Definition of the 'owl' sprite which has 24x21 pixel
// Each pixel is one bit in the matrix. Note: The pixels
// are not directly below each other, lines seem to 
// be shifted by half a pixel. As a consequence, 
// vertical lines are not quite shifted.
unsigned char MartinSprite[63]={
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0xff,0x00,
0x00,0x99,0x00,
0x00,0xff,0x00,
0x0f,0xff,0xf0,
0x0f,0xff,0xf0,
0x00,0xff,0x00,
0x00,0xff,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00};

/* 
 Process table to have a function that periodically moves a sprite
 In this process table there is only one entry, the second one has
 to be 0. The pointer and periodicity are assigned in main()
*/
struct process proctab[] = {{0,0}, {0,0}};
/*
 There is only a stub for the assembly language Rectangle() in the C library.
 Geos registers need to be filled manually. This function takes over the
 additional job to end up with the same function as is available in the
 assembly API.
*/
void DrawRectangle (char pattern, int x1, char y1, int x2, char y2) {

    // Lets draw a rectangle. Only a C-stub for the jump table exists so we have 
    // to fill the Geos registers manually.
    SetPattern(pattern);
    // Left top (x/y)
    r3 = x1;
    r2L = y1;
    // Right bottom (x/y)
    r4 = x2;
    r2H = y2;
    Rectangle();
    
}


/* 
 Menu handlers

 These functions are declared before the menu struct below which includes 
 pointers to these functions. The functions are then called when the
 user clicks on a menu item. 
*/

void smenu1 (void) {
    ReDoMenu();
    DlgBoxOk(CITALICON CBOLDON "Demo App 2018" CPLAINTEXT, "by Martin Sauter");
    return;
}

void smenu2 (void) {
    EnterDeskTop();
}

// Menu strucutre with pointers to the menu handlers above

static struct menu subMenu = {
        { 0, 13, 0, 53 },
        2 | HORIZONTAL,
          {
            { "geos", MENU_ACTION, smenu1 },
            { "exit", MENU_ACTION, smenu2 },
          }
 };

 // Let's define the window we're operating
struct window wholeScreen = {0, SC_PIX_HEIGHT-1, 0, SC_PIX_WIDTH-1};

void reinitScreen(void) {

    // Let's clear the screen - with different pattern, because apps have cleared screen upon
    // start
    SetPattern(2);
    InitDrawWindow(&wholeScreen);
    Rectangle();

} 

/**
 * myProcessHandler()
 *
 * When the application is started the Geos Process functionality is used
 * to call this function periodically (e.g. 60 times a second). It will
 * change the sprite location.
 * 
 * @param none
 *
 * @return none
 *
 */


void myProcessHandler(void) {

    static char direction = 0;
    // Sprite location that is updated in the periodic process function
    static struct pixel mainSpriteLoc = {50,100};
    
    if (mainSpriteLoc.x > 200) direction = 1;
    
    if (mainSpriteLoc.x < 30) direction = 0;

    if (direction == 0){
        mainSpriteLoc.x += 1;
    } else {
        mainSpriteLoc.x -= 1;
    }

    PosSprite(5, &mainSpriteLoc);

    return;
}


/**
 * myKeyInputHandler()
 *
 * This function is linked to the Geos keyboard vector and
 * thus called whenever the user presses a key. It checks
 * which key was pressed and outputs the PETSCII value
 * of the key to the screen.
 * 
 * @param none
 *
 * @return none
 *
 */

void myKeyInputHandler(void) {

    char keyPressed;
    char str[4];
        
    keyPressed = keyData;
    
    // Convert the key number to a string
    itoa ((int) keyPressed, str, 10); //convert to string

    // Delete previous output, make background white and write new output   
    DrawRectangle(0, 60, 183, 90, 193);   
    PutString (str, 190, 70);

    return;
}


/**
 * main()
 *
 * Initializes the screen, creates and activates the sprite and hooks callback 
 * functions for periodic sprite movement and keyboard input into the system 
 * so the functions are called periodically or event based.
 * Once done it returns to the Geos main loop, which never returns.
 *
 * @param none
 *
 * @return none
 *
 */

void main (void)
{
    char str[40];
    int i = 3300;
    struct pixel mPixel;

    itoa (i, str, 10); // let's convert an int to string

    reinitScreen();

    DoMenu(&subMenu);

    // Draw the whit rectangle in which the sprite moves
    DrawRectangle(0, 30, 20, 300, 180);

    // Position and activate the main sprite
    mPixel.x = 10;
    mPixel.y = 50;
    DrawSprite(5, MartinSprite);
    PosSprite(5, &mPixel);
    EnablSprite(5);
    
    // Show a dialog box with info
    /*
    DlgBoxOk(CITALICON CBOLDON "Hello, world 22" CPLAINTEXT, str);
    reinitScreen();
    DoMenu(&subMenu);
    */
    
    PutString (" Some Text For The Top Of The Screen 4 ", 10, 80);
    
    // Put a horizontal line at the bottom of the screen just to demonstrate how
    // to draw a horizontal line
    HorizontalLine (0xff, 190, 120, 200);
 
    // Initialize key input handler
    keyVector = &myKeyInputHandler;   

    // Initialize and start a process to move the sprite
    proctab[0].pointer = (unsigned) &myProcessHandler;
    proctab[0].jiffies = 1; // once every 1/50 or 1/60 (?) second
    InitProcesses(1, proctab);
    RestartProcess(0);  

    MainLoop();
}


