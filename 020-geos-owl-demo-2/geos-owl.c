/**
 * A demo program to show how the cc65 C API functions for GEOS 64
 * can be used.
 *
 * This program uses, among others, the following GEOS API functions 
 * to demonstrate their use:
 *
 *  - Menus and Menu handlers
 *  - Dialog Box call
 *  - Sprite and sprite movement
 *  - Output text
 *  - Draw lines and rectangles
 *  - Uses GEOS process function to periodically call a function
 *    that, e.g. moves the sprite to a new position on the screen
 *  - React to keyboard input
 *  - React to mouse button press, read mouse location
 *  - Use of some functions of the C stdlib, e.g. to convert
 *    and int to a string.
 *  - Detect and handle sprite collisions
 *
 * @version    1.0 2018-08-15
 * @package    DRDB
 * @copyright  Copyright (c) 2017 Martin Sauter
               martin.sauter@wirelessmoves.com
 * @license    GNU General Public License
 * @since      Since Release 1.0
 *
 */

#include <stdlib.h>
#include <string.h>
#include <geos.h>


// Memory locations not present in geos.h
#define mouseXPos         (*(unsigned*)0x003a)
#define mouseYPos         (*(char*)0x003c)

// Memory locations required for sprite collision checking
#define sprCollisionReg   (*(char*)0xd01e)
#define bankCtrl          (*(char*)0x0001)

// The last bit in the mouseData contains the info if the 
// mouse button is pressed
#define MOUSE_BTN_DOWN    128

// CBM key + x (ALT+x in emulator) to exit the application
#define CBM_X             248

// Define the screen area in which the sprite moves
#define LEFT_X             20
#define LEFT_Y             20
#define RIGHT_X           300
#define RIGHT_Y           180

// Coordinates to output text on the screen
#define TXT_X_START        20
#define TXT_Y_START        40

// Sprite dimension and used border pixels
#define SPRT_SIZE_X        24
#define SPRT_SIZE_Y        21
#define UNUSED_PXL_X        4
#define UNUSED_PXL_Y_TOP    6
#define UNUSED_PXL_Y_BTM    8

// Direction of movement of the main sprite
#define MOVES_LEFT          1
#define MOVES_RIGHT         0
#define MOVES_UP            1
#define MOVES_DOWN          0

// Speed of movement control
#define MIN_MOVE_DELAY      1
#define MAX_MOVE_DELAY      5

// Keys to change sprite speed
#define KEY_SPEED_X_RIGHT  30  // Arrow right
#define KEY_SPEED_X_LEFT    8  // Arrow left
#define KEY_SPEED_Y_UP     16  // Arrow up
#define KEY_SPEED_Y_DOWN   17  // Arrow down


// Sprite numbers. If changed, adapt SPRITE_COLLISION_MASK below!
#define SPRITE_MAIN_NUM        5
#define SPRITE_STAR_NUM        6

/* 
 * Bit combination to use to check for sprite collision
 * e.g. for sprite nums 5 and 6 (which are bits 6 and 7
 * if the first bit is bit 1...)
 * the bit combination is 01100000 = 96 decimal
 *
 * (Could be calculated in the program but let's be as 
 *  quick as possible by just using a fixed mask)
 *
 * Update if sprite numbers above change
 */
#define SPRITE_COLLISION_MASK 96

/* 
 * Definition of the main 'owl' sprite which has 24x21 pixel
 * Each pixel is one bit in the matrix.
 *
 * When changing the sprite, adapt the 'unused' pixel
 * definitions above so the sprite is still reflected
 * correctly at the playfield boundaries
 *
 */
 
unsigned char MyMainSprite[63]={
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0xff,0x00,
0x00,0x99,0x00,
0x00,0xff,0x00,
0x0f,0xff,0xf0,
0x0f,0xff,0xf0,
0x00,0xff,0x00,
0x00,0xff,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00};

/*
 * Definition of the 'star' sprite the owl has to reach
 *
 */

unsigned char MyStarSprite[63]={
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0xff,0x00,
0x0f,0xff,0xf0,
0x0f,0xff,0xf0,
0x00,0xff,0x00,
0x00,0xff,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00,
0x00,0x00,0x00};

// Variable to save the key that was pressed in the key handler function.
// The variable is then accessed in the periodic function handler.
char MyKeyPressed = 0;

// Sprites locatiosn that are initialized in main() and later updated in 
// the periodic process function
struct pixel mainSpriteLoc = {50, 100};
struct pixel mStarSpriteLoc = {70, 70};

// Declare functions prototypes that are called when menu items are 
// clicked on and are used in the struct that defines the menu below.
void MyMenuHandlerGeos (void);
void MyMenuHandlerExit (void);

// Menu structure with pointers to the menu handlers above
static struct menu subMenu = {
        { 0, 13, 0, 53 },
        2 | HORIZONTAL,
          {
            { "geos", MENU_ACTION, MyMenuHandlerGeos },
            { "exit", MENU_ACTION, MyMenuHandlerExit },
          }
 };

/** 
 *
 * MyMenuHandlerGeos()
 * MyMenuHandlerExit()
 *
 * These functions are called when the user clicks on a menu item. 
 *
 * @param none
 *
 * @return none
 *
 */

void MyMenuHandlerGeos (void) {
    ReDoMenu();
    DlgBoxOk(CITALICON CBOLDON "Geos Owl - Demo App 2018" CPLAINTEXT, "by Martin Sauter");
    return;
}

void MyMenuHandlerExit (void) {
    EnterDeskTop();
}


/**
 * MyReinitScreen()
 *
 * Initially and after showing a dialog box and other GUI elements
 * the screen needs to be reinitialized. This function does the basic
 * job that always has to be done.
 * 
 * @param none
 *
 * @return none
 *
 */
 
void MyReinitScreen(void) {

    // Let's define the window we're operating in
    struct window wholeScreen = {0, SC_PIX_HEIGHT-1, 0, SC_PIX_WIDTH-1};

    // Let's clear the screen - with different pattern, because apps have 
    // cleared screen upon start
    SetPattern(2);
    InitDrawWindow(&wholeScreen);
    Rectangle();

} 

/**
 * MyDrawRectangle()
 *
 * There is only a stub for the assembly language Rectangle() in the C library.
 * X and Y coordinates need to be put into Geos pseudo-registers manually. 
 * This function takes over the additional job to end up with the same function 
 * as is available in the assembly API. 
 * 
 * @param none
 *
 * @return none
 *
 */
 
void MyDrawRectangle (char pattern, int x1, char y1, int x2, char y2) {

    // Lets draw a rectangle. Only a C-stub for the jump table exists so we have 
    // to fill the Geos registers manually.
    SetPattern(pattern);
    // Left top (x/y)
    r3 = x1;
    r2L = y1;
    // Right bottom (x/y)
    r4 = x2;
    r2H = y2;
    Rectangle();
    
}

/**
 * MyMouseClickHandler()
 *
 * This function is called when the left mouse key is pressed
 * and released. mouseData, mouseXPos and mouseYPos contain
 * information about the key state and mouse pointer location.
 *
 * Bit 7 (last bit) of mouseData is 0 when the left mouse key
 * was pressed and 1 when it was released.
 * 
 * @param none
 *
 * @return none
 *
 */

void MyMouseClickHandler(void) {

    unsigned int x;
    char y;
    char strX[4];
    char strY[4];
    char combinedStr[31] = "";

    // Don't act on mouse button release
    if ((mouseData & MOUSE_BTN_DOWN) != 0) return;
        
    x = mouseXPos;
    y = mouseYPos;
      
    // Convert the key number to a string
    itoa ((int) x, strX, 10); //convert to string
    itoa ((int) y, strY, 10); //convert to string

    strcat (combinedStr, "Mouse Pressed at: ");
    strcat (combinedStr, strX);
    strcat (combinedStr, ", ");
    strcat (combinedStr, strY);    
    
    // Delete previous output, make background white and write new output   
    MyDrawRectangle(0, TXT_X_START, TXT_Y_START-7, TXT_X_START+130, TXT_Y_START+3);
    PutString (combinedStr, TXT_Y_START, TXT_X_START);
    
    return;
}

/**
 * MyKeyInputHandler()
 *
 * This function is linked to the Geos keyboard vector and
 * thus called whenever the user presses a key. It checks
 * which key was pressed and outputs the PETSCII value
 * of the key to the screen. It also saves the key that
 * was pressed in a global variable so it can be used
 * in other parts of the program as well.
 * 
 * @param none
 *
 * @return none
 *
 */

void MyKeyInputHandler(void) {

    char str[4];
    char combinedStr[26] = "Last key pressed: ";
    
    MyKeyPressed = keyData;
    
    // Exit if ALT + X was pressed
    if (MyKeyPressed == CBM_X) EnterDeskTop();
    
    // Convert the key number to a string
    itoa ((int) MyKeyPressed, str, 10); //convert to string

    // Delete previous output, make background white and write new output   
    strcat (combinedStr, str);    
    MyDrawRectangle(0, TXT_X_START, TXT_Y_START+10-7, TXT_X_START+110, TXT_Y_START+10+3);
    PutString (combinedStr, TXT_Y_START+10, TXT_X_START);

    return;
}


/**
 * myHandleSpriteCollisions()
 *
 * Called by myMoveMainSprite() after the sprite is moved to a new location
 * to check if that move has triggered a Sprite collisions. If so it
 * performs the actions to be done when the main sprite and the star sprite
 * meet. 
 *
 * Note: Sprite collisions with the mouse pointer are ignored by this function!
 *
 * To read the sprite collision register it's required to map the I/O space
 * bank into the memory map, as by default, the memory map at this location
 * is used for RAM for GEOS kernal code.
 * 
 * The standard way to get to IO space is to use the Geos Functions 
 * InitForIO() and DoneWithIO(). This works but also disables the
 * Sprites between the calls. In some regions of the screen (but not all!?)
 * the owl sprite then starts flickering.
 * 
 * As a consequence I chose to directly set the appropriate bits in the bank
 * control register (bankCtrl) which is at memory addres 0x0001 to 
 * map I/O space into the memory map, read the sprite collision register and
 * then restore the memory mapping to what was there before. 
 *
 * Note: I checked how the Geos kernel does IO when activating sprites and it 
 * is done in this way as well. Therefore it should be safe... 
 * See https://www.pagetable.com/?p=869
 * 
 * @param none
 *
 * @return none
 *
 */

void myCheckAndActoOnSpriteCollisions(void) {

    char tmpBankCtrl;
    static char localSprCollisonRegister = 0;
    static unsigned int numSpriteCollisions = 0;
    static char prevCollisionVal = 0;
    char str[6];
    char strCnt[7];
    char combinedStr[47] = "Sprite collision value and count: "; // 34 chars + 3 + 5 + 2 + 1 = 45

    tmpBankCtrl = bankCtrl; // Backup the current value of the bank register
    bankCtrl = 0x35;        // This value maps IO space into the memory map
    localSprCollisonRegister = sprCollisionReg;
    bankCtrl = tmpBankCtrl; // Restore original bank configuration

    // Act on a sprite collision but only the first time
    if ((localSprCollisonRegister != 0) && (prevCollisionVal == 0)) {
        numSpriteCollisions++;
       
        // save current collision value so we only treat it once and wait
        // until the collision clears again. The same value later will trigger
        // the function again.
        prevCollisionVal = localSprCollisonRegister;
       
        itoa(localSprCollisonRegister, str, 10);
        itoa(numSpriteCollisions, strCnt, 10);
       
        strcat(combinedStr, str);
        strcat(combinedStr, ", ");
        strcat(combinedStr, strCnt);
        MyDrawRectangle(0, TXT_X_START, TXT_Y_START+20-7, TXT_X_START+220, TXT_Y_START+20+3);
        PutString (combinedStr, TXT_Y_START+20, TXT_X_START);
        
    } else {
    
        // Clear the previous collision value only once the sprites are apart
        // from each other
        if (prevCollisionVal != localSprCollisonRegister) prevCollisionVal = 0;
    }

    return;
}


/**
 * myCheckAndActoOnKeyPresses()
 *
 * Called by MyProcessHandler() to periodically check if a key was pressed
 * and act on it.
 *
 * This function does not detect the keypress itself but relies on 
 * MyKeyInputHandler() to do this which is called by the OS when a key is 
 * pressed. The key id is then saved in a global variable which is used in
 * this function. After running this function the key id is set to 0 again.
 * 
 * @param char char x and y direction (by value)
 * @param char x and y delay before move (by reference) as these are updated
 *
 * @return variables given by reference are updated.
 *
 */
 
void myCheckAndActoOnKeyPresses(char xDirection, char yDirection, 
                                char *pxDelayBeforeMove, char *pyDelayBeforeMove) {
  
    if (MyKeyPressed != 0) {
    
        // Note: The following rules just allow acceleration in the direction of movement
        // and slowing down to the lowest possbile speed. They don't allow the sprite
        // to come to a standstill and accelerate in the other direction. Direction can
        // only be changed by hitting the playfield boundary!
        
        if (MyKeyPressed == KEY_SPEED_X_RIGHT) {
            if (xDirection == MOVES_RIGHT && *pxDelayBeforeMove > MIN_MOVE_DELAY) (*pxDelayBeforeMove)--;
            if (xDirection == MOVES_LEFT  && *pxDelayBeforeMove < MAX_MOVE_DELAY) (*pxDelayBeforeMove)++;
        }
        
        if (MyKeyPressed == KEY_SPEED_X_LEFT) {
            if (xDirection == MOVES_RIGHT && *pxDelayBeforeMove < MAX_MOVE_DELAY) (*pxDelayBeforeMove)++;
            if (xDirection == MOVES_LEFT  && *pxDelayBeforeMove > MIN_MOVE_DELAY) (*pxDelayBeforeMove)--;
        }

        if (MyKeyPressed == KEY_SPEED_Y_UP) {
            if (yDirection == MOVES_UP   && *pyDelayBeforeMove > MIN_MOVE_DELAY) (*pyDelayBeforeMove)--;
            if (yDirection == MOVES_DOWN && *pyDelayBeforeMove < MAX_MOVE_DELAY) (*pyDelayBeforeMove)++;
        }

        if (MyKeyPressed == KEY_SPEED_Y_DOWN) {
            if (yDirection == MOVES_UP   && *pyDelayBeforeMove < MAX_MOVE_DELAY) (*pyDelayBeforeMove)++;
            if (yDirection == MOVES_DOWN && *pyDelayBeforeMove > MIN_MOVE_DELAY) (*pyDelayBeforeMove)--;
        }
    
        MyKeyPressed = 0;

    }
    
    return;
}

/**
 * myMoveMainSprite()
 *
 * Contains the logic to move the main 'owl' sprite. The function checks
 * if a play field boundary was hit and if so inverses the x or y speed
 * direction. If the sprite is moved, a further function in this module is
 * called that checks if a sprite collision with the star sprite has occurd
 * and acts on it.
 *
 * @param char, the current x and y direction (by reference)
 * @param char, the remaining delays for x and y direction before moving the sprite
 *              (by reference)
 * @param char, the initial delay values for x and y direction (speed of the sprite)
 *              used to reinitialize the delay time after the sprite is moved by 
 *              a pixel
 *
 * @return The function changes the x and y direction of the sprite and the remaining
 *         x and y delays in the variables given by reference.
 *
 */

void myMoveMainSprite (char *pxDirection, char *pyDirection, 
                       char *pxRemainingDelayBeforeMove, char *pyRemainingDelayBeforeMove,
                       char xDelayBeforeMove, char yDelayBeforeMove) {

    char hasLocChanged = FALSE;

    //
    // Change directions if requried
    //

    // Change x direction when the sprite hits the left or right border
    if (mainSpriteLoc.x > (RIGHT_X - SPRT_SIZE_X + UNUSED_PXL_X)) *pxDirection = MOVES_LEFT;   
    if (mainSpriteLoc.x < (LEFT_X - UNUSED_PXL_X)) *pxDirection = MOVES_RIGHT;
    
    // Change the y direction when the sprite hits the top or bottom border
    if (mainSpriteLoc.y > (RIGHT_Y - SPRT_SIZE_Y + UNUSED_PXL_Y_BTM)) *pyDirection = MOVES_UP;   
    if (mainSpriteLoc.y < (LEFT_Y - UNUSED_PXL_Y_TOP)) *pyDirection = MOVES_DOWN;

    //
    // Process x movement
    //
    
    // If delay has not yet expired, decrease delay and do not move the sprite 
    if (*pxRemainingDelayBeforeMove > MIN_MOVE_DELAY) {    
      (*pxRemainingDelayBeforeMove)--;
    }
    else // Delay has expired, move the sprite
    {   

       if (*pxDirection == MOVES_RIGHT){
           mainSpriteLoc.x += 1;
       } else {
           mainSpriteLoc.x -= 1;
       }
      
       *pxRemainingDelayBeforeMove = xDelayBeforeMove;
       hasLocChanged = TRUE;
    }

    //
    // Process y movement
    //

    // If delay has not yet expired, decrease delay and do not move the sprite
    if (*pyRemainingDelayBeforeMove > MIN_MOVE_DELAY) {    
      (*pyRemainingDelayBeforeMove)--;
    }
    else // Delay has expired, move the sprite
    {     
        if (*pyDirection == MOVES_DOWN){
            mainSpriteLoc.y += 1;
        } else {
            mainSpriteLoc.y -= 1;
        }
        
        *pyRemainingDelayBeforeMove = yDelayBeforeMove;
        hasLocChanged = TRUE;
    }

    // Change the location of the sprite on the screen  
    if (hasLocChanged == TRUE) {
        
        PosSprite(SPRITE_MAIN_NUM, &mainSpriteLoc);
        
        // Check if the there is a sprite collision (with the star sprite)
        // and if so act on it.
        myCheckAndActoOnSpriteCollisions();
    }
}

/**
 * MyProcessHandler()
 *
 * When the application is started the Geos Process functionality is used
 * to call this function periodically (e.g. 60 times a second). It will
 * change the sprite location.
 * 
 * @param none
 *
 * @return none
 *
 */

void MyProcessHandler(void) {

    static char xDirection = MOVES_RIGHT;
    static char yDirection = MOVES_UP;

    static char xRemainingDelayBeforeMove = MAX_MOVE_DELAY;
    static char yRemainingDelayBeforeMove = MAX_MOVE_DELAY;
    
    static char xDelayBeforeMove = MAX_MOVE_DELAY;
    static char yDelayBeforeMove = MAX_MOVE_DELAY;    
    
    // Move the sprite 
    myMoveMainSprite (&xDirection, &yDirection, 
                      &xRemainingDelayBeforeMove, &yRemainingDelayBeforeMove,
                      xDelayBeforeMove, yDelayBeforeMove);

    // Key presses can change the x and y speed of the main sprite
    myCheckAndActoOnKeyPresses(xDirection, yDirection, &xDelayBeforeMove, 
                               &yDelayBeforeMove);
    
    return;
}


/**
 * main()
 *
 * Initializes the screen, creates and activates the sprite and hooks callback 
 * functions for periodic sprite movement and keyboard input into the system 
 * so the functions are called periodically or event based.
 * Once done it returns to the Geos main loop, which never returns.
 *
 * @param none
 *
 * @return none
 *
 */

void main (void)
{

    /* 
     Process table to have a function that periodically moves a sprite
     In this process table there is only one entry, the second one has
     to be 0. The pointer and periodicity are assigned in main()
    */
    static struct process proctab[] = {{0,0}, {0,0}};

    char str[40];
    int i = 3300;  // an Integer to test integer to string conversion

    i = i / 256;
    itoa (i, str, 10); // let's convert an int to string

    // Initialize the screen after program startup
    MyReinitScreen();
    DoMenu(&subMenu);
    
    // Show a dialog box with info
    /*    
    DlgBoxOk(CITALICON CBOLDON "Hello, world 23" CPLAINTEXT, str);
    MyReinitScreen();
    DoMenu(&subMenu);
    */    

    // Draw the white rectangle in which the sprite moves
    MyDrawRectangle(0, LEFT_X, LEFT_Y, RIGHT_X, RIGHT_Y);
    
    // Activate the main sprite (initial position in struct definition above)
    DrawSprite(SPRITE_MAIN_NUM, MyMainSprite);
    PosSprite(SPRITE_MAIN_NUM, &mainSpriteLoc);
    EnablSprite(SPRITE_MAIN_NUM);
    
    // Activate the star sprite  (initial position in struct definition above)
    DrawSprite(SPRITE_STAR_NUM, MyStarSprite);
    PosSprite(SPRITE_STAR_NUM, &mStarSpriteLoc);
    EnablSprite(SPRITE_STAR_NUM);    
    
    PutString (" The GEOS Owl Demo Program - Version 20 ", 10, 80);
    
    // Put a horizontal line at the bottom of the screen just to demonstrate how
    // to draw a horizontal line
    HorizontalLine (0xff, 190, 120, 200);
 
    // Initialize key input handler
    keyVector = &MyKeyInputHandler;   
    
    // Initialize the mouse click handler
    otherPressVec = &MyMouseClickHandler;

    // Initialize and start a process to move the sprite
    proctab[0].pointer = (unsigned) &MyProcessHandler;
    proctab[0].jiffies = 1; // once every 1/50 or 1/60 (?) second
    InitProcesses(1, proctab);
    RestartProcess(0);  
    
    // Never returns    
    MainLoop();
}


